package com.yabushan.form.domain;

import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 自定义表单对象 form_infos
 *
 * @author yabushan
 * @date 2021-06-26
 */
public class FormInfos extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 表单名称 */
    @Excel(name = "表单名称")
    private String formRef;

    /** 表单模型 */
    @Excel(name = "表单模型")
    private String formModel;

    /** 大小 */
    @Excel(name = "大小")
    private String size;

    /** 标签位置 */
    @Excel(name = "标签位置")
    private String labelPosition;

    /** 标签长度 */
    @Excel(name = "标签长度")
    private String labelWidth;

    /** 表单规则 */
    @Excel(name = "表单规则")
    private String formRules;

    /** gutter */
    @Excel(name = "gutter")
    private String gutter;

    /** 是否禁用表单 */
    @Excel(name = "是否禁用表单")
    private String disalbed;

    /** span */
    @Excel(name = "span")
    private String span;

    /** 表单按钮 */
    @Excel(name = "表单按钮")
    private String formBtns;

    /** json表单 */
    @Excel(name = "json表单")
    private String fields;

    /** 表单状态 */
    @Excel(name = "表单状态")
    private String formStatus;

    /** 表单类型 */
    @Excel(name = "表单类型")
    private String formType;

    /** 表单关联工作流 */
    @Excel(name = "表单关联工作流")
    private String formWorkflow;

    /** 备注 */
    @Excel(name = "备注")
    private String formMemo;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setFormRef(String formRef)
    {
        this.formRef = formRef;
    }

    public String getFormRef()
    {
        return formRef;
    }
    public void setFormModel(String formModel)
    {
        this.formModel = formModel;
    }

    public String getFormModel()
    {
        return formModel;
    }
    public void setSize(String size)
    {
        this.size = size;
    }

    public String getSize()
    {
        return size;
    }
    public void setLabelPosition(String labelPosition)
    {
        this.labelPosition = labelPosition;
    }

    public String getLabelPosition()
    {
        return labelPosition;
    }
    public void setLabelWidth(String labelWidth)
    {
        this.labelWidth = labelWidth;
    }

    public String getLabelWidth()
    {
        return labelWidth;
    }
    public void setFormRules(String formRules)
    {
        this.formRules = formRules;
    }

    public String getFormRules()
    {
        return formRules;
    }
    public void setGutter(String gutter)
    {
        this.gutter = gutter;
    }

    public String getGutter()
    {
        return gutter;
    }
    public void setDisalbed(String disalbed)
    {
        this.disalbed = disalbed;
    }

    public String getDisalbed()
    {
        return disalbed;
    }
    public void setSpan(String span)
    {
        this.span = span;
    }

    public String getSpan()
    {
        return span;
    }
    public void setFormBtns(String formBtns)
    {
        this.formBtns = formBtns;
    }

    public String getFormBtns()
    {
        return formBtns;
    }
    public void setFields(String fields)
    {
        this.fields = fields;
    }

    public String getFields()
    {
        return fields;
    }
    public void setFormStatus(String formStatus)
    {
        this.formStatus = formStatus;
    }

    public String getFormStatus()
    {
        return formStatus;
    }
    public void setFormType(String formType)
    {
        this.formType = formType;
    }

    public String getFormType()
    {
        return formType;
    }
    public void setFormWorkflow(String formWorkflow)
    {
        this.formWorkflow = formWorkflow;
    }

    public String getFormWorkflow()
    {
        return formWorkflow;
    }
    public void setFormMemo(String formMemo)
    {
        this.formMemo = formMemo;
    }

    public String getFormMemo()
    {
        return formMemo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("formRef", getFormRef())
                .append("formModel", getFormModel())
                .append("size", getSize())
                .append("labelPosition", getLabelPosition())
                .append("labelWidth", getLabelWidth())
                .append("formRules", getFormRules())
                .append("gutter", getGutter())
                .append("disalbed", getDisalbed())
                .append("span", getSpan())
                .append("formBtns", getFormBtns())
                .append("fields", getFields())
                .append("formStatus", getFormStatus())
                .append("formType", getFormType())
                .append("formWorkflow", getFormWorkflow())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("formMemo", getFormMemo())
                .toString();
    }
}
