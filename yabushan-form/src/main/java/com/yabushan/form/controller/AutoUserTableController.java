package com.yabushan.form.controller;

import java.util.List;

import com.yabushan.common.utils.SecurityUtils;
import com.yabushan.form.domain.FiledInsertInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.form.domain.AutoUserTable;
import com.yabushan.form.service.IAutoUserTableService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 *  动态表信息Controller
 *
 * @author yabushan
 * @date 2021-08-06
 */
@RestController
@RequestMapping("/form/autotable")
public class AutoUserTableController extends BaseController
{
    @Autowired
    private IAutoUserTableService autoUserTableService;

    /**
     * 查询 动态表信息列表
     */
    @PreAuthorize("@ss.hasPermi('form:autotable:list')")
    @GetMapping("/list")
    public TableDataInfo list(AutoUserTable autoUserTable)
    {
        startPage();
        String username = SecurityUtils.getUsername();
        if(!username.equals("admin")){
            autoUserTable.setCreatedBy(username);
        }
        List<AutoUserTable> list = autoUserTableService.selectAutoUserTableList(autoUserTable);
        return getDataTable(list);
    }

    /**
     * 导出 动态表信息列表
     */
    @PreAuthorize("@ss.hasPermi('form:autotable:export')")
    @Log(title = " 动态表信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AutoUserTable autoUserTable)
    {
        List<AutoUserTable> list = autoUserTableService.selectAutoUserTableList(autoUserTable);
        ExcelUtil<AutoUserTable> util = new ExcelUtil<AutoUserTable>(AutoUserTable.class);
        return util.exportExcel(list, "autotable");
    }

    /**
     * 获取 动态表信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('form:autotable:query')")
    @GetMapping(value = "/{bId}")
    public AjaxResult getInfo(@PathVariable("bId") String bId)
    {
        return AjaxResult.success(autoUserTableService.selectAutoUserTableById(bId));
    }

    /**
     * 新增 动态表信息
     */
    @PreAuthorize("@ss.hasPermi('form:autotable:add')")
    @Log(title = " 动态表信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AutoUserTable autoUserTable)
    {
        int i = autoUserTableService.insertAutoUserTable(autoUserTable);
        if(i==100){
            //超过允许建表数量
            return AjaxResult.error("超过允许建表数量,请联系管理员提升等级！");
        }else if(i==-1){
            return AjaxResult.error("查无VIP数据，请联系管理员新增建表权限！");
        }
        return toAjax(i);
    }

    /**
     * 修改 动态表信息
     */
    @PreAuthorize("@ss.hasPermi('form:autotable:edit')")
    @Log(title = " 动态表信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AutoUserTable autoUserTable)
    {

        int i = autoUserTableService.updateAutoUserTable(autoUserTable);
        if(i==100){
            //超过允许建表数量
            return AjaxResult.error("超过允许建表数量,请联系管理员提升等级！");
        }else if(i==-1){
            return AjaxResult.error("查无VIP数据，请联系管理员新增建表权限！");
        }
        return toAjax(i);
    }

    /**
     * 删除 动态表信息
     */
    @PreAuthorize("@ss.hasPermi('form:autotable:remove')")
    @Log(title = " 动态表信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{bIds}")
    public AjaxResult remove(@PathVariable String[] bIds)
    {
        return toAjax(autoUserTableService.deleteAutoUserTableByIds(bIds));
    }


    //动态插入或更新一个字段数据
    /**
     * 新增 动态表信息
     */
    @PreAuthorize("@ss.hasPermi('form:autotable:add')")
    @Log(title = " 动态表字段数据", businessType = BusinessType.INSERT)
    @PostMapping("/addFiledValue")
    public AjaxResult addFiledValue(@RequestBody FiledInsertInfo filedInsertInfo)
    {
       return toAjax(autoUserTableService.addFiledValue(filedInsertInfo));
    }




}
