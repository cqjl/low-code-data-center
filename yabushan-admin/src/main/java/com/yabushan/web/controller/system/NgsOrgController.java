package com.yabushan.web.controller.system;

import java.util.List;

import com.yabushan.system.utils.OrgTreeSelect;
import com.yabushan.system.utils.UumOrgInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.NgsOrg;
import com.yabushan.system.service.INgsOrgService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 班级Controller
 *
 * @author yabushan
 * @date 2021-06-05
 */
@RestController
@RequestMapping("/system/org")
public class NgsOrgController extends BaseController
{
    @Autowired
    private INgsOrgService ngsOrgService;

    /**
     * 查询班级列表
     */
    @PreAuthorize("@ss.hasPermi('system:org:list')")
    @GetMapping("/list")
    public TableDataInfo list(NgsOrg ngsOrg)
    {
        if("1".equals(ngsOrg.getParentId())){
            ngsOrg.setParentId(null);
        }
        startPage();
        List<NgsOrg> list = ngsOrgService.selectNgsOrgList(ngsOrg);
        return getDataTable(list);
    }

    /**
     * 导出班级列表
     */
    @PreAuthorize("@ss.hasPermi('system:org:export')")
    @Log(title = "班级", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(NgsOrg ngsOrg)
    {
        List<NgsOrg> list = ngsOrgService.selectNgsOrgList(ngsOrg);
        ExcelUtil<NgsOrg> util = new ExcelUtil<NgsOrg>(NgsOrg.class);
        return util.exportExcel(list, "org");
    }

    /**
     * 获取班级详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:org:query')")
    @GetMapping(value = "/{orgId}")
    public AjaxResult getInfo(@PathVariable("orgId") String orgId)
    {
        return AjaxResult.success(ngsOrgService.selectNgsOrgById(orgId));
    }

    /**
     * 新增班级
     */
    @PreAuthorize("@ss.hasPermi('system:org:add')")
    @Log(title = "班级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NgsOrg ngsOrg)
    {
        return toAjax(ngsOrgService.insertNgsOrg(ngsOrg));
    }

    /**
     * 修改班级
     */
    @PreAuthorize("@ss.hasPermi('system:org:edit')")
    @Log(title = "班级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NgsOrg ngsOrg)
    {
        return toAjax(ngsOrgService.updateNgsOrg(ngsOrg));
    }

    /**
     * 删除班级
     */
    @PreAuthorize("@ss.hasPermi('system:org:remove')")
    @Log(title = "班级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{orgIds}")
    public AjaxResult remove(@PathVariable String[] orgIds)
    {
        return toAjax(ngsOrgService.deleteNgsOrgByIds(orgIds));
    }


    @GetMapping("/treeselect")
    @ApiOperation(value = "获取部门下拉树列表")
    public AjaxResult treeselect(NgsOrg dept)
    {
        List<NgsOrg> depts = ngsOrgService.selectNgsOrgList(dept);
        List<OrgTreeSelect> orgTreeSelects = ngsOrgService.buildDeptTreeSelect(depts);
        return AjaxResult.success(orgTreeSelects);
    }
}
