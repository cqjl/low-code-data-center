package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxGiftInfo;
import com.yabushan.system.service.IYmxGiftInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 礼物Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxgiftinfo")
public class YmxGiftInfoController extends BaseController
{
    @Autowired
    private IYmxGiftInfoService ymxGiftInfoService;

    /**
     * 查询礼物列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxgiftinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxGiftInfo ymxGiftInfo)
    {
        startPage();
        List<YmxGiftInfo> list = ymxGiftInfoService.selectYmxGiftInfoList(ymxGiftInfo);
        return getDataTable(list);
    }

    /**
     * 导出礼物列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxgiftinfo:export')")
    @Log(title = "礼物", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxGiftInfo ymxGiftInfo)
    {
        List<YmxGiftInfo> list = ymxGiftInfoService.selectYmxGiftInfoList(ymxGiftInfo);
        ExcelUtil<YmxGiftInfo> util = new ExcelUtil<YmxGiftInfo>(YmxGiftInfo.class);
        return util.exportExcel(list, "ymxgiftinfo");
    }

    /**
     * 获取礼物详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxgiftinfo:query')")
    @GetMapping(value = "/{giftId}")
    public AjaxResult getInfo(@PathVariable("giftId") String giftId)
    {
        return AjaxResult.success(ymxGiftInfoService.selectYmxGiftInfoById(giftId));
    }

    /**
     * 新增礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxgiftinfo:add')")
    @Log(title = "礼物", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxGiftInfo ymxGiftInfo)
    {
        return toAjax(ymxGiftInfoService.insertYmxGiftInfo(ymxGiftInfo));
    }

    /**
     * 修改礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxgiftinfo:edit')")
    @Log(title = "礼物", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxGiftInfo ymxGiftInfo)
    {
        return toAjax(ymxGiftInfoService.updateYmxGiftInfo(ymxGiftInfo));
    }

    /**
     * 删除礼物
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxgiftinfo:remove')")
    @Log(title = "礼物", businessType = BusinessType.DELETE)
	@DeleteMapping("/{giftIds}")
    public AjaxResult remove(@PathVariable String[] giftIds)
    {
        return toAjax(ymxGiftInfoService.deleteYmxGiftInfoByIds(giftIds));
    }
}
