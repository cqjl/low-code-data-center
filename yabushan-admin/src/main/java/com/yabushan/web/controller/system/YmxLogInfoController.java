package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxLogInfo;
import com.yabushan.system.service.IYmxLogInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 日志Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxloginfo")
public class YmxLogInfoController extends BaseController
{
    @Autowired
    private IYmxLogInfoService ymxLogInfoService;

    /**
     * 查询日志列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxloginfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxLogInfo ymxLogInfo)
    {
        startPage();
        List<YmxLogInfo> list = ymxLogInfoService.selectYmxLogInfoList(ymxLogInfo);
        return getDataTable(list);
    }

    /**
     * 导出日志列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxloginfo:export')")
    @Log(title = "日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxLogInfo ymxLogInfo)
    {
        List<YmxLogInfo> list = ymxLogInfoService.selectYmxLogInfoList(ymxLogInfo);
        ExcelUtil<YmxLogInfo> util = new ExcelUtil<YmxLogInfo>(YmxLogInfo.class);
        return util.exportExcel(list, "ymxloginfo");
    }

    /**
     * 获取日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxloginfo:query')")
    @GetMapping(value = "/{logId}")
    public AjaxResult getInfo(@PathVariable("logId") String logId)
    {
        return AjaxResult.success(ymxLogInfoService.selectYmxLogInfoById(logId));
    }

    /**
     * 新增日志
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxloginfo:add')")
    @Log(title = "日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxLogInfo ymxLogInfo)
    {
        return toAjax(ymxLogInfoService.insertYmxLogInfo(ymxLogInfo));
    }

    /**
     * 修改日志
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxloginfo:edit')")
    @Log(title = "日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxLogInfo ymxLogInfo)
    {
        return toAjax(ymxLogInfoService.updateYmxLogInfo(ymxLogInfo));
    }

    /**
     * 删除日志
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxloginfo:remove')")
    @Log(title = "日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{logIds}")
    public AjaxResult remove(@PathVariable String[] logIds)
    {
        return toAjax(ymxLogInfoService.deleteYmxLogInfoByIds(logIds));
    }
}
